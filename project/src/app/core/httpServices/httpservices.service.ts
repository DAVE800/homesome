import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HttpservicesService {

  constructor(private http:HttpClient) { }


  add(data:any){
    return this.http.post<any>(environment.BaseApi+"api/v1/add/",data)
  }

  get(){
    return this.http.get<any>(environment.BaseApi+"api/v1/");
  }


}
