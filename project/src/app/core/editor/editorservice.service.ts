import { Injectable } from '@angular/core';
import { fabric } from 'fabric';
import {Subject } from 'rxjs';
declare var require: any
var myalert=require('sweetalert2')

declare var FontFace:any;
declare var require: any
@Injectable({
  providedIn: 'root'
})
export class EditorserviceService {

  addedText=new Subject<any>();

  constructor() { }


  MakeImage=(url:any,canvas:any)=>{
   /**
    *  var img = new Image();
    img.src=url
    img.crossOrigin ='anonymous';
    img.onload = ()=> {
    var Img = new fabric.Image(img);
    canvas.setBackgroundImage(Img,canvas.renderAll.bind(canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:0,
            top:5,
            right:5,
            bottom:0,    
            scaleX: canvas.width / img.width,
            scaleY: canvas.height /img.height
            
        });
        canvas.centerObject(Img);
        canvas.renderAll(Img);
        canvas.requestRenderAll(); 
        
      }
   */
      var img =url;
    fabric.Image.fromURL(img,(img:any) => {
     canvas.add(img).setActiveObject(img);
    canvas.centerObject(img);
    canvas.renderAll(img);
    },
    {
       scaleX:0.5,     
       scaleY:0.5,
       crossOrigin: "Anonymous",
    
       
    }
    );
  }
  



  addText=(canvas:any,message:any=null)=>{
    if(message){

      if(canvas.getActiveObject().text){
        canvas.getActiveObject().text=""

        canvas.getActiveObject().text=message
        
        canvas.requestRenderAll();
      }else{
        let text= new fabric.Textbox("custom text",{
          top:200,
          left:200,
          fill:"blue",
          fontSize:38,
          fontStyle:'normal',
          cornerStyle:'circle',
          selectable:true,
          borderScaleFactor:1,
          overline:false,
          lineHeight:1.5
        });
        
        canvas.add(text).setActiveObject(text);
        canvas.renderAll(text);
        canvas.requestRenderAll();
        canvas.centerObject(text);

      }
    
     
    }else{
      let text= new fabric.Textbox("custom text",{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:1.5
      });
      
      canvas.add(text).setActiveObject(text);
      canvas.renderAll(text);
      canvas.requestRenderAll();
      canvas.centerObject(text);
    }
  
    
    

  }

  overline=(canvas:any)=>{
    let item = canvas.getActiveObject()
    if(item.type=="activeSelection"){
      for(let i of item){
        if(!i.overline){
          i.set({overline:true})
          canvas.renderAll(i);
          canvas.requestRenderAll()
        }else{
          i.set({overline:false});
          canvas.renderAll(i);
          canvas.requestRenderAll()
        }
      }
    }
    
    if(item.type!="activeSelection"){
        if(!item.overline){
          item.set({overline:true});
          canvas.renderAll(item);
          canvas.requestRenderAll()
        }else{
          item.set({overline:false});
          canvas.renderAll(item);
          canvas.requestRenderAll()
        }
      
    }
  }


  underline=(canvas:any)=>{
    let item=canvas.getActiveObject()
    if(item && item.type!="activeSelection"){
      if(!item.underline){
        item.set({underline:true});
        canvas.renderAll(item);
        
  
      }else{
        item.set({underline:false});
        canvas.renderAll(item);
        canvas.requestRenderAll();

    
      }
    }
  
    if(item && item.type=="activeSelection"){
  
      for(let el of item._objects){
        if(!item.underline){
  
          el.set({underline:true});
          canvas.renderAll(el);
  
        }else{
          el.set({underline:false});
          canvas.renderAll(el);

        }
      }
      canvas.requestRenderAll();

    }
  }


  italic=(canvas:any)=>{
    let item= canvas.getActiveObject();
    if(item!=undefined && item.type=="activeSelection"){
      for(let el of item._objects){
        if(el.fontStyle!="normal"){
          el.set({fontStyle:'normal'});
          canvas.renderAll(el)
         
        }else{
          el.set({fontStyle:'italic'});
          canvas.renderAll(el);
        }
      }
      canvas.requestRenderAll();

      
    }
    if(item && item!="activeSelection"){
      if(item.fontStyle!="normal"){
        item.set({fontStyle:"normal"});
        canvas.renderAll(item);
      }else{
        item.set({
          fontStyle:"italic"
        });
      canvas.renderAll(item);
      canvas.requestRenderAll();

      }
    }
  }


  bold=(canvas:any)=>{
    let item= canvas.getActiveObject();
  if(item!=undefined && item.type=="activeSelection"){    
      for(let el of item._objects){
        if(el.fontWeight=="normal"){
        el.set({fontWeight:'bold'});
        canvas.renderAll(el);
      }else{
          el.set({fontWeight:'normal'});
          canvas.renderAll(el);  

      }
      canvas.requestRenderAll();  

    }
  }
    
  if(item!=undefined && item.type!="activeSelection"){
    if(item.fontWeight=="normal"){   
      item.set({fontWeight:'bold'});
        canvas.renderAll(item);
        canvas.requestRenderAll();
     
    }else{
      item.set({fontWeight:'normal'});
        canvas.renderAll(item);
        canvas.requestRenderAll();   
    }

  }
  }

  remove=(canvas:any)=>{
    let item = canvas.getActiveObject();
    if(item && item.type!="activeSelection"){
      canvas.remove(item);
      canvas.requestRenderAll();

    } 
    if(item && item.type=="activeSelection"){
      for(let e of item._objects){
      canvas.remove(e);
      canvas.requestRenderAll();

      }
    }
  }




  textbefore=(canvas:any)=>{
    let text = canvas.getActiveObject()
    if(text && text.type!="activeSelection"){ 
      if(text.underline && text._textBeforeEdit){
        text.set({text:text._textBeforeEdit,underline:false});
        canvas.renderAll(text);
  
      }   
      if(text._textBeforeEdit){
        text.set({text:text._textBeforeEdit});
        canvas.renderAll(text);

  
      }
      canvas.requestRenderAll();


    } 
  
    if(text && text.type=="activeSelection"){
      for(let el of text._objects){
        if(el.underline && el._textBeforeEdit){
          el.set({text:el._textBeforeEdit,underline:false});
          canvas.renderAll(el);
  
        }   
        if(el._textBeforeEdit){
          el.set({text:el._textBeforeEdit});
          canvas.renderAll(el);

        }
  
      }
      canvas.requestRenderAll();

    }
  }


  textfont=(event:any,canvas:any)=>{
 const myfont :any= new FontFace(event.name,`url(${event.url})`);
  myfont.load().then((loaded:any)=>{
    (document as any).fonts.add(loaded)
    let item= canvas.getActiveObject();

    if( item &&  item.type=="activeSelection"){
      for(let el of  item._objects){
        el.set({
          fontFamily:event.name
        });
        canvas.renderAll(el);
         canvas.requestRenderAll();
  
      }
  
    }
  
    if( item &&  item.type!="activeSelection"){
      item.set({
        fontFamily:event.name
      });
      canvas.renderAll(item);
      canvas.requestRenderAll();
    }

  }).catch((err:any)=>{
    console.log(err);
    
  })
  }


  copy=(canvas:any)=>{
    canvas.getActiveObject().clone(function(cloned:any) {
      canvas._clipboard= cloned;
    });
  }

  paste=(canvas:any)=>{
    canvas._clipboard.clone(function(clonedObj:any) {
      canvas.discardActiveObject();
      clonedObj.set({
        left: clonedObj.left + 15,
        top: clonedObj.top + 15,
        evented: true,
      });
      if (clonedObj.type === 'activeSelection') {
        clonedObj.canvas = canvas;
        clonedObj.forEachObject(function(obj:any) {
          canvas.add(obj);
        });
        clonedObj.setCoords();
      } else {
        canvas.add(clonedObj);
      }
      canvas._clipboard.top += 15;
      canvas._clipboard.left += 15;
      canvas.setActiveObject(clonedObj);
      canvas.requestRenderAll();
    });
  }


  textcolor=(color:any,canvas:any)=>{
    let item= canvas.getActiveObject();
    if(item && item.type!="activeSelection"){
      item.set({fill:color});
      canvas.renderAll(item);
      canvas.requestRenderAll();

    }
 
    if(item && item.type=="activeSelection"){
      for(let el of item._objects){
        el.set({fill:color});
        canvas.renderAll(el);
      }
      canvas.requestRenderAll();

     }
  }


  setitem=(event:any,canvas:any)=>{
    var img = event.target;
    img.setAttribute("crossOrigin",'anonymous')   
    fabric.Image.fromURL(img.src,(img:any) => {
     canvas.add(img).setActiveObject(img);
    canvas.centerObject(img);
    canvas.renderAll(img);
    },
    {
       scaleX:0.5,     
       scaleY:0.5,
       crossOrigin: "Anonymous",
    
       
    }
    );
    
  }



  handleChanges(file:any):Boolean {
    var fileTypes = [".png",".jpeg",".jpg"];
    var filePath = file.name;
    if (filePath) {
                 var filePic = file; 
                 var fileType = filePath.slice(filePath.indexOf(".")); 
                 var fileSize = file.size; //Select the file size
                 if (fileTypes.indexOf(fileType) == -1) { 
                         myalert.fire({
                          title: "Erreur",
                          text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                          icon: "error",
                           button: "Ok"
                          });
                      return true;
                 }
  
        if (+fileSize > 200*200) {
          myalert.fire({
            title: "Erreur",
            text: `importer un fichier de taille ${200}x${200} MG!`,
            icon: "error",
             button: "Ok"
            });
            return true;
        }
  
        var reader = new FileReader();
        reader.readAsDataURL(filePic);
        reader.onload =  (e:any) =>{
            var data = e.target.result;
                         
            var image = new Image();
            image.onload = ():any =>{
                var width = image.width;
                var height = image.height;
                if(width == 200 && height == 200) {                        
                  return false ;
                   
                } else {
                     myalert.fire({
                      title: "Erreur",
                      text: `la taille doit etre ${200}x${200}`,
                      icon: "error",
                       button: "Ok"
                      });
                    return true ;
                }
  
  
            };
          };
    } else {
  
        return true;
        
    }
  
    return false;
  }

  
  
}
